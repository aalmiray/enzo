/*
 * Copyright (c) 2015 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.hansolo.enzo.gauge;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


/**
 * User: hansolo
 * Date: 29.12.13
 * Time: 07:56
 */
public class DemoOneEightyGauge extends Application {
    private OneEightyGauge gauge;
    
    @Override public void init() {
        gauge = OneEightyGaugeBuilder.create()
                                     .title("Temperature")
                                     .unit("°C")
                                     .maxValue(40)                  
                                     //.barBackgroundColor(Color.BLACK)
                                     .barColor(Color.web("#5399c6"))
                                     //.valueColor(Color.DARKVIOLET)
                                     //.unitColor(Color.CRIMSON)
                                     //.minTextColor(Color.BLUE)
                                     //.maxTextColor(Color.RED)
                                     //.titleColor(Color.ORANGE)
                                     .build();        
    }

    @Override public void start(Stage stage) {        
        StackPane pane = new StackPane();
        pane.setPadding(new Insets(10, 10, 10, 10));
        pane.getChildren().addAll(gauge);

        Scene scene = new Scene(pane);

        stage.setScene(scene);
        stage.show();
        
        gauge.setValue(20);
    }

    @Override public void stop() {

    }

    public static void main(String[] args) {
        launch(args);
    }
}
